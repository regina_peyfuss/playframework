name := "ScalaRestAPI"
 
version := "1.0" 
      
lazy val `scalarestapi` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
      
resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"

resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"

scalaVersion := "2.12.2"

libraryDependencies ++= Seq( jdbc , ehcache , ws ,  guice )

libraryDependencies ++= Seq(
  "org.joda" % "joda-convert" % "1.8"
)

libraryDependencies ++= Seq(
  "net.logstash.logback" % "logstash-logback-encoder" % "4.9"
)

libraryDependencies ++= Seq(
  "com.netaporter" %% "scala-uri" % "0.4.16"
)


libraryDependencies ++= Seq(
  "net.codingwell" %% "scala-guice" % "4.1.0"
)

libraryDependencies ++= Seq(
  "org.reactivemongo" %% "reactivemongo" % "0.12.6"
)

libraryDependencies ++= Seq(
  "org.scalactic" %% "scalactic" % "3.0.4"
)
libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.0.4" % "test"
)

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

      