package controllers.unit


import org.scalatest._

class HomeControllerSpec extends UnitSpec {

  "An Index Action" should "return a message containing REST API home page" in {
    val message = "REST API home page";
    assert(message.indexOf("REST API") > -1)
  }
}
