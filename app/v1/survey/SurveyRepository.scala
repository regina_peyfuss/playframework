package v1.survey

import javax.inject.{Inject, Singleton}

import akka.actor.ActorSystem
import play.api.libs.concurrent.CustomExecutionContext
import play.api.{Logger, MarkerContext}

import scala.concurrent.Future

final case class SurveyData(id: SurveyId, age: SurveyAge, gender: String)

class SurveyId private (val underlying: Int) extends AnyVal {
  override def toString: String = underlying.toString
}

class SurveyAge private (val underlying:Int) extends AnyVal {
  override def toString: String = underlying.toString
}

object SurveyId {
  def apply(raw: String): SurveyId = {
    require(raw != null)
    new SurveyId(Integer.parseInt(raw))
  }
}

object SurveyAge {
  def apply(raw: String): SurveyAge = {
    require(raw != null)
    new SurveyAge(Integer.parseInt(raw))
  }
}

class SurveyExecutionContext @Inject()(actorSystem: ActorSystem) extends CustomExecutionContext(actorSystem, "repository.dispatcher")

/**
  * A pure non-blocking interface for the PostRepository.
  */
trait SurveyRepository {
  def create(data: SurveyData)(implicit mc: MarkerContext): Future[SurveyId]

  def list()(implicit mc: MarkerContext): Future[Iterable[SurveyData]]

  def get(id: SurveyId)(implicit mc: MarkerContext): Future[Option[SurveyData]]
}

/**
  * A trivial implementation for the Survey Repository.
  *
  * A custom execution context is used here to establish that blocking operations should be
  * executed in a different thread than Play's ExecutionContext, which is used for CPU bound tasks
  * such as rendering.
  */
@Singleton
class SurveyRepositoryImpl @Inject()()(implicit ec: SurveyExecutionContext) extends SurveyRepository {

  private val logger = Logger(this.getClass)

  private val surveyList = List(
    SurveyData(SurveyId("1"), SurveyAge("28"), "male"),
    SurveyData(SurveyId("2"), SurveyAge("45"), "male"),
    SurveyData(SurveyId("3"), SurveyAge("31"), "female"),
    SurveyData(SurveyId("4"), SurveyAge("20"), "female"),
    SurveyData(SurveyId("5"), SurveyAge("27"), "male")
  )

  override def list()(implicit mc: MarkerContext): Future[Iterable[SurveyData]] = {
    Future {
      logger.trace(s"list: ")
      surveyList
    }
  }

  override def get(id: SurveyId)(implicit mc: MarkerContext): Future[Option[SurveyData]] = {
    Future {
      logger.trace(s"get: id = $id")
      surveyList.find(survey => survey.id == id)
    }
  }

  def create(data: SurveyData)(implicit mc: MarkerContext): Future[SurveyId] = {
    Future {
      logger.trace(s"create: data = $data")
      data.id
    }
  }

}
