package v1.survey


import javax.inject.Inject

import play.api.Logger
import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

case class SurveyFormInput(age: String, gender: String)

class SurveyController @Inject()(cc: SurveyControllerComponents)(implicit ec: ExecutionContext)
  extends SurveyBaseController(cc){
    private val logger = Logger(getClass)
    private val form: Form[SurveyFormInput] = {
      import play.api.data.Forms._
      Form(
        mapping(
          "age" -> nonEmptyText,
          "gender" -> text
        )(SurveyFormInput.apply)(SurveyFormInput.unapply)
      )
    }

    def index: Action[AnyContent] = SurveyAction.async { implicit request =>
      logger.trace("index: ")
      surveyResourceHandler.find.map { surveys =>
        Ok(Json.toJson(surveys))
      }
    }
  def process: Action[AnyContent] = SurveyAction.async { implicit request =>
    logger.trace("process: ")
    processJsonPost()
  }

  def show(id: String): Action[AnyContent] = SurveyAction.async { implicit request =>
    logger.trace(s"show: id = $id")
    surveyResourceHandler.lookup(id).map { survey =>
      Ok(Json.toJson(survey))
    }
  }

  private def processJsonPost[A]()(implicit request: SurveyRequest[A]): Future[Result] = {
    def failure(badForm: Form[SurveyFormInput]) = {
      Future.successful(BadRequest(badForm.errorsAsJson))
    }

    def success(input: SurveyFormInput) = {
      surveyResourceHandler.create(input).map { survey =>
        Created(Json.toJson(survey)).withHeaders(LOCATION -> survey.link)
      }
    }

    form.bindFromRequest().fold(failure, success)
  }
}
