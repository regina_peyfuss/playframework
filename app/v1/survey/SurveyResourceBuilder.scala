package v1.survey
import javax.inject.{Inject, Provider}

import play.api.MarkerContext

import scala.concurrent.{ExecutionContext, Future}
import play.api.libs.json._

/**
  * DTO for displaying survey information.
  */
case class SurveyResource(id: String, link: String, age: String, gender: String)

object SurveyResource {
  /**
    * Mapping to write a SurveyResource out as a JSON value.
    */
  implicit val implicitWrites = new Writes[SurveyResource] {
    def writes(survey: SurveyResource): JsValue = {
      Json.obj(
        "id" -> survey.id,
        "link" -> survey.link,
        "age" -> survey.age,
        "gender" -> survey.gender
      )
    }
  }
}

/**
  * Controls access to the backend data, returning [[SurveyResource]]
  */
class SurveyResourceHandler @Inject()(
                                     routerProvider: Provider[SurveyRouter],
                                     surveyRepository: SurveyRepository)(implicit ec: ExecutionContext) {

  def create(surveyInput: SurveyFormInput)(implicit mc: MarkerContext): Future[SurveyResource] = {
    val data = SurveyData(SurveyId("999"), SurveyAge("22"), surveyInput.gender)
    // We don't actually create the post, so return what we have
    surveyRepository.create(data).map { id =>
      createSurveyResource(data)
    }
  }

  def lookup(id: String)(implicit mc: MarkerContext): Future[Option[SurveyResource]] = {
    val surveyFuture = surveyRepository.get(SurveyId(id))
    surveyFuture.map { maybeSurveyData =>
      maybeSurveyData.map { SurveyData =>
        createSurveyResource(SurveyData)
      }
    }
  }

  def find(implicit mc: MarkerContext): Future[Iterable[SurveyResource]] = {
    surveyRepository.list().map { surveyDataList =>
      surveyDataList.map(surveyData => createSurveyResource(surveyData))
    }
  }

  private def createSurveyResource(p: SurveyData): SurveyResource = {
    SurveyResource(p.id.toString, routerProvider.get.link(p.id), p.age.toString, p.gender)
  }

}